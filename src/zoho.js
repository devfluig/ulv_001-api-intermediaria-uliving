const fs = require('fs')
const path = require('path')
const request = require('./request.js')
const { getAllResourceValues, checkRoundRobin } = require('./getResources.js')
const loadSettings = require('./utils').loadSettings

const leadsURL = "https://www.zohoapis.com/crm/v2/Leads"

const setOwner = (leadEmail, ownerEmail) => {
    const body = {
        "data": [
            {
                "Owner": {
                    "email": ownerEmail
                },
                "Email": leadEmail
            }
        ]
    }

    return request('post', `${leadsURL}/upsert`, body)
        .then(res => {
            res = res.data
            if (res.status === 'error') throw new Error(res.details)
            return res.status
        })
        .catch(err => {
            return Promise.resolve({
                status: 500,
                error: err
            })
        })
}

const zoho = (app) => {
    app.post("/owner/set", async (req, res) => {
        const leadEmail = req.body.leadEmail
        const region = req.body.region?.normalize("NFD")?.replace(/[\u0300-\u036f]/g, '')?.toLowerCase()

        if (!leadEmail || !region)
            return res.status(400).send({
                status: 400,
                error: 'Missing required fields: leadEmail and/or region'
            })

        await getAllResourceValues()
            .then(async (response) => {
                checkRoundRobin(response)
                const saveFilePAth = path.join(__dirname, '..', 'files', 'round-robin.json')

                const roundRobinObj = loadSettings(saveFilePAth)

                let regionExists = false
                for (const [key, value] of Object.entries(response)) {

                    if (key === region) {
                        if (value.length === 0) break

                        let next = value.length - 1 <= roundRobinObj[key] ? 0 : roundRobinObj[key] + 1
                        roundRobinObj[key] = next
                        regionExists = true
                        break
                    }
                }

                let ownerEmail = null

                if (!regionExists) {
                    let next = response.default.length - 1 <= roundRobinObj.default ? 0 : roundRobinObj.default + 1
                    roundRobinObj.default = next
                    ownerEmail = response.default[next]
                } else {
                    const index = roundRobinObj[region]
                    ownerEmail = response[region][index]
                }

                if (!ownerEmail) throw new Error('Couldn\'t find the owner email. Resources response: ' + response)

                const setOwnerResponse = await setOwner(leadEmail, ownerEmail)

                fs.writeFileSync(saveFilePAth, JSON.stringify(roundRobinObj))
                res.status(200).send(setOwnerResponse)
            })
            .catch(err => {
                res.status(500).send({
                    status: 500,
                    error: err
                })
            })
    })
}

module.exports = zoho
