const fs = require('fs')

const loadSettings = (path, array = false) => {
    try{
        const dataBuffer = fs.readFileSync(path)
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    }
    catch(e){
        if(array) return []
        return {}
    }
}

module.exports = {
    loadSettings
}
