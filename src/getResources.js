const axios = require('axios')
const fs = require('fs')
const path = require('path')
const loadSettings = require('./utils').loadSettings
const { v4: uuidv4 } = require('uuid')

const BOT_AUTH_KEY = process.env.BOT_AUTH_KEY

const getAllResources = () => {
    return axios({
        method: 'post',
        url: "https://msging.net/commands",
        headers: {
            'Content-Type': 'application/json',
            'Authorization': BOT_AUTH_KEY
        },
        data: {
            "id": uuidv4(),
            "method": "get",
            "uri": "/resources"
        }
    })
        .then(res => {
            res = res.data
            return res.resource.items
        })
        .catch(err => {
            return Promise.resolve({
                status: 500,
                error: err
            })
        })
}

const getAllResourceValues = async () => {
    const resourceKeys = await getAllResources()

    let obj = {}

    await Promise.all(resourceKeys.map(async (key) => {
        key = key.replace(' ', '%20')
        return await axios({
            method: 'post',
            url: "https://msging.net/commands",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': BOT_AUTH_KEY
            },
            data: {
                "id": uuidv4(),
                "method": "get",
                "uri": `/resources/${key}`
            }
        })
            .then(res => {
                key = key.replace('%20', ' ')
                res = res.data
                obj[key.normalize("NFD").replace(/[\u0300-\u036f]/g, '').toLowerCase()] = res.resource.split(',').map(i => i.trim()).filter(i => i?.length)  // Key minúscula e sem acentos.
                return res.resource
            })
            .catch(err => {
                console.log('ERRO ao retornar conteúdo do recurso: ')
                console.log(err)
                return ''
            })
    }))

    return obj
}

const checkRoundRobin = (resourcesObj) => {
    const saveFilePAth = path.join(__dirname, '..', 'files', 'round-robin.json')

    const file = loadSettings(saveFilePAth)
    for (const [key, value] of Object.entries(resourcesObj)) {

        if (!file.hasOwnProperty(key))
            file[key] = 0
    }

    //console.log('file: ' + JSON.stringify(file))

    fs.writeFileSync(saveFilePAth, JSON.stringify(file))
    return true
}

const getResources = (app) => {

    app.get("/resources", async (req, res) => {

        await getAllResourceValues()
            .then(response => {
                checkRoundRobin(response)

                res.status(200).send(response)
            })
            .catch(err => {
                res.status(500).send({
                    status: 500,
                    error: err
                })
            })
    })
}

module.exports = {
    getResources,
    getAllResourceValues,
    checkRoundRobin
}
