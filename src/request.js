const axios = require("axios")
const qs = require("qs")

const refresh_token = process.env.REFRESH_TOKEN
const client_id = process.env.CLIENT_ID
const client_secret = process.env.CLIENT_SECRET


// Refresh token
const new_token = () => {
    return axios({
        method: "post",
        url: "https://accounts.zoho.com/oauth/v2/token",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        data: qs.stringify({
            refresh_token: refresh_token,
            grant_type: "refresh_token",
            client_id: client_id,
            client_secret: client_secret
        }),
    });
};

const newRequest = (method, url, body) => {
    url = url.replace(/%40/g, "@");

    const request = (header) => {
        let promise = null;

        switch (method) {
            case "get":
                promise = axios({
                    method: "get",
                    url: url,
                    headers: header,
                });
                break;

            case "post":
                promise = axios({
                    method: "post",
                    url: url,
                    headers: header,
                    data: body,
                });
                break;
        }

        return promise;
    };

    if (!body && method === "post")
        return { error: "Body not found.", status: 400, description: "" };

    return new_token().then((resToken) => {
        const data = resToken?.data;

        if (!data?.access_token)
            return {
                error: "New token request failed.",
                status: 511,
                description:
                    "The refresh token provided in the API is no longer valid.",
            };

        return request({
            Authorization: `Zoho-oauthtoken ${data.access_token}`,
        }).then((response) => {
            if (response.error)
                return { error: "Resource failed.", status: 500, description: "" };
            return response.data;
        });
    });
};


module.exports = newRequest
