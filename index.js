const dotenv = require("dotenv").config({ silent: true })

const express = require("express");
const newRequest = require('./src/request.js')

const port = process.env.PORT || 3000;

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

require('./src/getResources.js').getResources(app)
require('./src/zoho.js')(app)

const leadsURL = "https://www.zohoapis.com/crm/v2/Leads";
const contactsURL = "https://www.zohoapis.com/crm/v2/Contacts";


app.get("/", (req, res) => {
  res.status(200).json({
    NAME: "API for integration between Zoho (Uliving) and Blip v1.1.0",
  });
});

// GET LEADS
app.get("/leads", (req, res) => {
  newRequest("get", leadsURL, undefined)
    .then((resGet) => {
      if (resGet.error) return res.status(resGet.status).json(resGet);

      console.log(`${req.ip} - Successfully got Leads information!`);

      if (!resGet.data) return res.status(204).json(resGet);
      res.status(200).json(resGet.data);
    })
    .catch((e) => {
      res.status(500).json({ error: "Internal server error.", description: e });
    });
});

// SEARCH LEAD BY EMAIL
app.get("/leads/:email", (req, res) => {
  const param = req.params.email.normalize("NFD").replace(/[\u0300-\u036f]/g, '').toLowerCase();

  newRequest("get", `${leadsURL}/search?email=${param}`, undefined)
    .then((resGet) => {
      if (resGet.error) return res.status(resGet.status).json(resGet);

      console.log(`${req.ip} - Successfully got Lead information!`);

      if (!resGet.data) return res.status(204).json(resGet);
      res.status(200).json(resGet.data[0]);
    })
    .catch((e) => {
      res.status(500).json({ error: "Internal server error.", description: e });
    });
});

// POST LEAD
app.post("/leads", (req, res) => {
  newRequest("post", leadsURL, req?.body)
    .then((resPost) => {
      if (resPost.error) return res.status(resPost.status).json(resPost);

      console.log(`${req.ip} - Successfully created a Lead!`);
      res.status(201).json(resPost.data[0]);
    })
    .catch((e) => {
      res.status(500).json({ error: "Internal server error.", description: e });
    });
});

// UPSERT LEAD
app.post("/leads/upsert", (req, res) => {

  newRequest("post", `${leadsURL}/upsert`, req?.body)
    .then((resPost) => {
      if (resPost.error) return res.status(resPost.status).json(resPost);

      console.log(
        `${req.ip} - Successfully altered a Lead: ${resPost.data[0]?.action ?? "create/insert"
        }`
      );
      res.status(201).json(resPost.data[0]);
    })
    .catch((e) => {
      res.status(500).json({ error: "Internal server error.", description: e });
    });
});

// GET CLIENT BY EMAIL
app.get("/clients/cliente/:email", (req, res) => {
  const param = req.params.email.normalize("NFD").replace(/[\u0300-\u036f]/g, '').toLowerCase();

  // Tem algum problema com queries com underscores na requisição direta ("/clients/cliente/:email").
  newRequest("get", contactsURL, undefined)
    .then((resGet) => {
      if (resGet.error) return res.status(resGet.status).json(resGet);
      if (!resGet.data) return res.status(204).json(resGet);

      const contact = resGet.data.filter((item) => {
        if (!item['Email']) return false;
        return item["Email"].normalize("NFD").replace(/[\u0300-\u036f]/g, '').toLowerCase() === param;
      });

      console.log(`${req.ip} - Successfully got Contacts information!`);

      if (contact.length > 0) return res.status(200).json(contact[0]);
      res.status(204).json({ error: "Contact not found.", description: "" });
    })
    .catch((e) => {
      res.status(500).json({ error: "Internal server error.", description: e });
    });
});

// GET ACCOUNTABLE BY EMAIL (RESPONSÁVEL FINANCEIRO)
app.get("/clients/responsavel/:email", (req, res) => {
  const param = req.params.email.normalize("NFD").replace(/[\u0300-\u036f]/g, '').toLowerCase();

  newRequest("get", `${contactsURL}/search?email=${param}`, undefined)
    .then((resGet) => {
      if (resGet.error) return res.status(resGet.status).json(resGet);
      if (!resGet.data) return res.status(204).json(resGet);

      const contact = resGet.data.filter((item) => {
        if (!item['E_mail_Respons_vel_financeiro']) return false;
        return item["E_mail_Respons_vel_financeiro"].normalize("NFD").replace(/[\u0300-\u036f]/g, '').toLowerCase() === param;
      });

      console.log(`${req.ip} - Successfully got Contacts information!`);

      if (contact.length > 0) return res.status(200).json(contact[0]);
      res.status(204).json({ error: "Contact not found.", description: "" });
    })
    .catch((e) => {
      res.status(500).json({ error: "Internal server error.", description: e });
    });
});

app.post("/clients", (req, res) => {
  return res.status(501).json({
    error: "Client creation not implemented.",
    description:
      "The server does not support the functionality required to fulfill the request",
  });
});

app.listen(port, () => {
  console.log(`Server is up on port ${port}`);
});
